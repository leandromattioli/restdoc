import parser;
import std.stdio;
import std.algorithm;

const string ARGHELP = "--help";

void usage(string binName) {
	writefln("Documentation extraction utility");
	writefln("Usage:");
	writefln("  %s %s", binName, ARGHELP);
	writefln("     Shows this help screen");
	writefln("  %s --srcdir srcpath --outdir outpath", binName);
	writefln("     Generates documentation from source files in srcpath.");
	writefln("     Outputs RST files to outpath.");
	writefln("  %s --file filepath", binName);
	writefln("     Generates documentation for filepath. Outputs to stdout.");
}

int main(string[] args) {
    if(args.length == 2 && !cmp(args[1], ARGHELP)) {
        usage(args[0]);
        return 0;
    }
    else if(args.length == 3 && !cmp(args[1], "--file")) {
        processFile(args[2]);
        return 0;
    }
    else if(args.length==5 && !cmp(args[1],"--srcdir") && !cmp(args[3],"--outdir")) {
        processDir(args[2], args[4]);
        return 0;
    }
    else {
        stderr.write("\n Incorrect usage! \n\n");
		usage(args[0]);
		return -1;
    }
}
