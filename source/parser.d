import std.stdio;
import std.string;
import std.file;
import std.c.stdlib;

const char PATHSEP='\\';

string[][] languages = [
  [".c", "///", "c"],
  [".cpp", "///", "c"],
  [".cs", "///", "csharp"],
  [".h", "///", "c"],
  [".java", "///", "java"],
  [".lsp", ";;;", "common-lisp"],
  [".php", "///", "php"],
  [".py", "###", "python"]
];


string[] listdir(string pathname) {
    import std.file;
    import std.path;
    import std.algorithm;
    import std.array;

    return std.file.dirEntries(pathname, SpanMode.shallow)
        .filter!(a => a.isFile)
        .map!(a => std.path.baseName(a.name))
        .array;
}


void processFile(string filePath) {
    string output;
    string token, language;
    if(!exists(filePath) || !isFile(filePath)) {
        stderr.writeln("Invalid source file!");
        exit(-1);
    }

    output = filePath ~ ".rst";
    token = "///";
    language = "c";

    foreach(entry; languages) {
        if(entry && filePath.endsWith(entry[0])) {
            token = entry[1];
            language = entry[2];
            break;
        }
    }

    auto fin = File(filePath, "r");
    parseFile(fin, stdout, token, language);
    fin.close();
}


void processDir(string srcdir, string outdir) {
    string source, output, token, language;
    if(!exists(srcdir) || !isDir(srcdir)) {
        stderr.writeln("Invalid path for source files!");
        exit(-1);
        return;
    }

    if(!exists(outdir) || !isDir(outdir)) {
        try {
            mkdir(outdir);
        }
        catch(FileException) {
            stderr.writeln("Can't access or create output dir!");
            exit(-1);
        }
    }
    
    writefln("Processing %s ... \n", srcdir);

    foreach(file; listdir(srcdir)) {
        source = srcdir;
        if(source[source.length - 1] != PATHSEP)
            source ~= PATHSEP;
        source ~= file;

        output = outdir;
        if(output[output.length - 1] != PATHSEP)
            output ~= PATHSEP;
        output ~= file ~ ".rst";

        token = "";
        language = "";

        foreach(entry; languages) {
            if(entry && source.endsWith(entry[0])) {
                token = entry[1];
                language = entry[2];
                break;
            }
        }

        if(token == "") {
            continue; //not a source file
        }

        writefln("Generating %s...", output);

        File fin, fout;
        fin.open(source, "r");
        fout.open(output, "w");
        parseFile(fin, fout, token, language);
        fin.close();
        fout.close();
    }
}



void parseFile(File fin, File fout, string token, string language) {
    bool flagcode = false;
    string strLine, strippedLine, token_space;
    
    token_space = token ~ " ";
    foreach(line; fin.byLine) {
        strLine = cast(string) line;
        if(strLine.startsWith(token)) {
            strippedLine = strLine.strip();
            strippedLine = strippedLine.replace(token_space, "");
            strippedLine = strippedLine.replace(token, "");
            if(flagcode) {
                fout.writeln();
                flagcode = false;
            }
            fout.writeln(strippedLine);
        }
        else {
            strippedLine = strLine.strip();
            if(strippedLine) {
                if(!flagcode) {
                    fout.writef("\n.. code:: %s\n\n", language);
                    flagcode = true;
                }
                fout.writefln("  %s", strLine);
            }
        }
    }
    fout.write("\r\n");
}
